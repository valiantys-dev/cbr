package com.valiantys.confluence.plugin;

import java.security.Permissions;
import java.util.ArrayList;
import java.util.Iterator;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.actions.AbstractPageAwareAction;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.spring.container.ContainerManager;
import com.opensymphony.xwork.interceptor.component.ComponentManager;

public class DeletePagesAction extends AbstractPageAwareAction {
	/** serial version UID */
	private static final long serialVersionUID = 1L;

	/** the page manager */
	private PageManager pageManager;

	/** The page to return after process */
	private Page pageToReturn;

	private boolean removeCurrent;

	/**
	 * Initialize the confirmation form
	 * 
	 * @return always success
	 * @throws Exception never
	 */
	public String doInit() throws Exception {
		String result = SUCCESS;
		if (!checkRight(getPage())) {
			pageToReturn = this.getPage();
			this.addActionError("remove.branch.error.right", new Object[] {});
			result = ERROR;
		}
		return result;
	}

	/**
	 * @see com.atlassian.confluence.pages.actions.AbstractPageAwareAction#getPage()
	 */
	public Page getPage() {
		return (Page) super.getPage();
	}

	/**
	 * Check if the current user has the delete right for the current page and
	 * all its childrens
	 * 
	 * @param page the current page
	 * @return true if the user has the right, false else.
	 */
	private boolean checkRight(Page page) {
//		if (GeneralUtil.isSuperUser(getRemoteUser()))
		if (this.permissionManager.hasPermission(getRemoteUser(), Permission.ADMINISTER, page.getSpace()))
			return true;

		boolean check = permissionManager.hasPermission(getRemoteUser(), Permission.VIEW, page)
				&& permissionManager.hasPermission(getRemoteUser(), Permission.REMOVE, page)
				&& permissionManager.hasPermission(getRemoteUser(), Permission.EDIT, page);
		for (Iterator<Page> it = page.getChildren().iterator(); it.hasNext() && check;) {
			check = checkRight(it.next());
		}
		return check;
	}

	/**
	 * @see com.opensymphony.xwork.ActionSupport#execute()
	 */
	public String execute() throws Exception {
		deleteChildrens(this.getPage());
		if (this.isRemoveCurrent()) {
			pageToReturn = this.getPage().getParent();
			pageManager.trashPage(this.getPage());
		} else {
			pageToReturn = this.getPage();
		}
		return SUCCESS;
	}

	/**
	 * Delete all the children and sub children of the current page
	 * 
	 * @param page The page to start
	 */
	private void deleteChildrens(Page page) {
		ArrayList<Page> pages = getPages(page);
		// delete children first
		for (Iterator<Page> it = pages.iterator(); it.hasNext();) {
			Page tmp = it.next();
			pageManager.trashPage(tmp);
		}
	}

	/**
	 * Get all the page to delete
	 * 
	 * @param page The page to start
	 */
	private ArrayList<Page> getPages(Page page) {
		ArrayList<Page> res = new ArrayList<Page>();
		for (Iterator<Page> it = page.getChildren().iterator(); it.hasNext();) {
			Page p = it.next();
			res.addAll(getPages(p));
			res.add(p);
		}
		return res;
	}

	/**
	 * Get the redirect URL after process
	 * 
	 * @return the URL
	 */
	public String getRedirectUrl() {
		// if no parent page, redirect to space's home
		if (pageToReturn == null) {
			pageToReturn = getSpace().getHomePage();
		}
		// if no home page either, redirect to browse space
		if (pageToReturn == null) {
			return "/pages/listpages-alphaview.action?key=" + GeneralUtil.urlEncode(getSpaceKey());
		}

		return GeneralUtil.getPageUrl(pageToReturn);
	}

	/**
	 * Get the page manager
	 * 
	 * @return the page manager
	 */
	public PageManager getPageManager() {
		return pageManager;
	}

	/**
	 * Set the page manager
	 * 
	 * @param pageManager the page manager
	 */
	public void setPageManager(PageManager pageManager) {
		this.pageManager = pageManager;
	}

	/**
	 * Check if the current page have to be removed
	 * 
	 * @return if the current page have to be removed
	 */
	public boolean isRemoveCurrent() {
		return removeCurrent;
	}

	/**
	 * Set if the current page have to be removed
	 * 
	 * @param removeCurrent if the current page have to be removed
	 */
	public void setRemoveCurrent(boolean removeCurrent) {
		this.removeCurrent = removeCurrent;
	}
}
